
import types from "./types";

const load = () => (dispatch, getState, services) => {
  dispatch({
    type: types.LOAD,
  });
  services.branchService
    .getBranches()
    .then(branches => dispatch({
      type: types.LOADED,
      payload: branches,
    }));
};

const select = key => ({
  type: types.SELECT,
  payload: key,
});

const initialize = key => (dispatch, getState, services) => {
  dispatch({
    type: types.INITIALIZE,
    payload: key,
  });
  services.branchService
    .initializeBranch(key)
    .then(branch => dispatch({
      type: types.INITIALIZED,
      payload: branch,
    }));
};

const remove = key => (dispatch, getState, services) => {
  dispatch({
    type: types.REMOVE,
    payload: key,
  });
  services.branchService
    .removeBranch(key)
    .then(branch => dispatch({
      type: types.REMOVED,
      payload: key,
    }));
};

const run = (key, artifactName) => (dispatch, getState, services) => {
  dispatch({
    type: types.RUN,
    payload: {
      key,
      artifactName,
    },
  });
  services.branchService
    .runBranch(key, artifactName)
    .then(branch => dispatch({
      type: types.RUNNING,
      payload: branch,
    }));
};

const stop = (key, artifactName) => (dispatch, getState, services) => {
  dispatch({
    type: types.STOP,
    payload: {
      key,
      artifactName,
    },
  });
  services.branchService
    .stopBranch(key, artifactName)
    .then(branch => dispatch({
      type: types.STOPPED,
      payload: branch,
    }));
};

export default {
  load,
  select,
  initialize,
  remove,
  run,
  stop,
};
