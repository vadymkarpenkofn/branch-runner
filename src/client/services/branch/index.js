
class BranchService {

  getBranches() {
    return fetch("/api/branch")
      .then(response => response.json());
  }

  initializeBranch(key) {
    return fetch(`/api/branch/${key}`, { method: "POST" })
      .then(response => response.json());
  }

  runBranch(key, artifactName) {
    return fetch(`/api/branch/${key}/${artifactName}`, { method: "POST" })
      .then(response => response.json());
  }

  stopBranch(key, artifactName) {
    return fetch(`/api/branch/${key}/${artifactName}`, { method: "DELETE" })
      .then(response => response.json());
  }

  removeBranch(key, artifactName) {
    return fetch(`/api/branch/${key}`, { method: "DELETE" })
      .then(response => response.json());
  }

}

export default BranchService;
