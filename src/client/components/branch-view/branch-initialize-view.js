import React from "react";
import { connect } from "react-redux";

import {
  actions as branchActions,
  selectors as branchSelectors,
} from "store/branch";

const BranchInitializeView = ({
  branch,
  initializeBranch,
}) => (
  <div className="has-text-centered">
    {branch ?
      <button
        className={`button is-large is-primary ${branch.isInitializing && "is-loading"}`}
        onClick={e => initializeBranch(branch.key)}
        >
        Initialize Branch
      </button>
      :
      <span>Loading &hellip;</span>
    }
  </div>
);

const mapState = state => ({
  branch: branchSelectors.selected(state),
});

const mapDispatch = dispatch => ({
  initializeBranch: key => dispatch(branchActions.initialize(key)),
});

export default connect(mapState, mapDispatch)(BranchInitializeView);
