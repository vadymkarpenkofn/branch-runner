import React from "react";
import { connect } from "react-redux";

import {
  selectors as branchSelectors,
} from "store/branch";

import BranchRunningView from "./branch-running-view";
import BranchInitializeView from "./branch-initialize-view";

const BranchView = ({
  branch,
}) => (
  <div className="BranchView">
    {branch && branch.created
      ? <BranchRunningView />
      : <BranchInitializeView />
    }
  </div>
);

const mapState = state => ({
  branch: branchSelectors.selected(state),
});

export default connect(mapState)(BranchView);
