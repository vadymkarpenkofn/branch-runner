import React from "react";
import { Switch, Route } from "react-router"

import Header from "components/header";
import BranchList from "components/branch-list";
import BranchView from "components/branch-view";

const BranchesPage = () =>
  <div className="BranchesPage">
    <Header />
    <section className="BranchesPage-content section">
      <div className="columns">
        <div className="column is-narrow">
          <BranchList />
        </div>
        <div className="column">
          <Switch>
            <Route
              path="/branches/:branchKey"
              component={BranchView}
            />
            <Route>
              <div className="has-text-centered">
                nothing to see here
              </div>
            </Route>
          </Switch>
        </div>
      </div>
    </section>
  </div>;

export default BranchesPage;
