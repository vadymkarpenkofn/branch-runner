import React from "react";
import { connect } from "react-redux";

import { actions as branchActions } from "store/branch";

import BranchListView from "./view";

class BranchList extends React.Component {

  state = {
    branches: [],
    searchTerm: "",
    filterType: "all",
  }

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.filter(this.props);
  }

  componentWillReceiveProps(props) {
    if (props.branches !== this.props.branches)
      this.filter(props);
  }

  onFilter(filterType) {
    this.setState({ filterType }, () =>
      this.filter(this.props)
    );
  }

  onSearch(searchTerm) {
    this.setState({ searchTerm }, () =>
      this.filter(this.props)
    );
  }

  filter(props) {
    const { filterType, searchTerm } = this.state;
    this.setState({
      branches: props.branches
        .filter(x => this.getFilter(filterType)(x))
        .filter(x => searchTerm ? x.name.toLowerCase().indexOf(searchTerm.toLowerCase()) >= 0 : true)
        .sort((a, b) => a.name == b.name ? 0 : a.name < b.name ? 1 : -1),
    });
  }

  getFilter(filterType) {
    switch (filterType) {
      case "running":
        return x => x.artifacts && x.artifacts.length;
      case "all":
      default:
        return x => true;
    }
  }

  render() {
    return BranchListView({
      ...this.props,
      ...this.state,
      onSearch: this.onSearch.bind(this),
      onFilter: this.onFilter.bind(this),
    });
  }
}

const mapState = state => ({
  isLoading: state.branch.isLoading,
  branches: state.branch.all,
  selected: state.branch.selected,
});

const mapDispatch = dispatch => ({
  loadBranches: () => dispatch(branchActions.load()),
  selectBranch: key => dispatch(branchActions.select(key)),
});

export default connect(mapState, mapDispatch)(BranchList);
