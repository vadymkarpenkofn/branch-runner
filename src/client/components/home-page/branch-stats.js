import React from "react";
import { connect } from "react-redux";

import { selectors as branchSelectors } from "store/branch";

const BranchStats = ({
  totalCount,
  latestCount,
  runningCount,
}) => (
  <div className="columns">
    <div className="column">
      <div className="has-text-centered">
        <h3 className="subtitle is-4">
          Running
        </h3>
        <h4 className="is-jumbo">
          {runningCount}
        </h4>
      </div>
    </div>
    <div className="column">
      <div className="has-text-centered">
        <h3 className="subtitle is-4">
          Available
        </h3>
        <h4 className="is-jumbo">
          {totalCount}
        </h4>
      </div>
    </div>
    <div className="column">
      <div className="has-text-centered">
        <h3 className="subtitle is-4">
          New
        </h3>
        <h4 className="is-jumbo">
          {latestCount}
        </h4>
      </div>
    </div>
  </div>
);

const mapState = state => ({
  totalCount: branchSelectors.totalCount(state),
  latestCount: branchSelectors.latestCount(state),
  runningCount: branchSelectors.runningCount(state),
});

export default connect(mapState)(BranchStats);
