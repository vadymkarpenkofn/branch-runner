import express from "express";

import ConfigService from "../services/config";
import RunnerService from "../services/runner";
import BambooService from "../services/bamboo";

const configService = new ConfigService();

const bambooConfig = {
  urlBase: configService.getVariable("bamboo.urlBase"),
  planKey: configService.getVariable("bamboo.planKey"),
};

if (!bambooConfig.urlBase || !bambooConfig.planKey)
  throw new Error("Missing Bamboo config! Please set BAMBOO_URLBASE and BAMBOO_PLANKEY.");

const bambooService = new BambooService(bambooConfig.urlBase, bambooConfig.planKey);
const runnerService = new RunnerService({ javaXms: "64M", javaXmx: "256M" });

const router = express.Router();

const errorHandler = (response, error) => {
  console.log(error);
  response
    .status(500)
    .send({ error });
};

router.get("/branch", (request, response) => {
  bambooService.getBranches()
    .then(branches => {
      const branchesWithStatus = branches.map(branch => ({
        ...branch,
        ...runnerService.loadBranch(branch.key),
      }));
      response.send(branchesWithStatus);
    })
    .catch(error => errorHandler(response, error));
});

router.post("/branch/:key", (request, response) => {
  const { key } = request.params;
  const success = branch =>
    response
      .status(201)
      .send(branch);
  const error = message =>
    response
      .status(400)
      .send({ message });
  const initializeBranch = branch =>
    runnerService
      .initializeBranch(branch)
      .then(success);
  bambooService
    .getResult(key)
    .then(initializeBranch)
    .catch(error => errorHandler(response, error));
});

router.delete("/branch/:key", (request, response) => {
  const { key } = request.params;
  runnerService
    .deleteBranch(key)
    .then(() => response.send({}))
    .catch(error => errorHandler(response, error));
});

router.post("/branch/:key/:build", (request, response) => {
  const { key, build } = request.params;
  runnerService
    .runArtifact(key, build)
    .then(branch => {
      response
        .status(200)
        .send(branch);
    })
    .catch(error => errorHandler(response, error));
});

router.delete("/branch/:key/:build", (request, response) => {
  const { key, build } = request.params;
  runnerService
    .stopArtifact(key, build)
    .then(branch => {
      response
        .status(200)
        .send(branch);
    })
    .catch(error => errorHandler(response, error));
});

export { router };
