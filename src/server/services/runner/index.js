import fs from "fs";
import { spawn } from "child_process";
import rimraf from "rimraf";
import fetch from "node-fetch";

const branchDir = key => `runner/${key}`;
const branchFile = key => `${branchDir(key)}/branch.json`;
const artifactFile = (key, name) => `${branchDir(key)}/${name}.jar`;

class RunnerService {

  _config = {
    killTimeoutSeconds: 1,
    runDelaySeconds: 8,
    javaXms: "64M",
    javaXmx: "64M",
    javaServerPortLow: 55000,
    javaServerPortHigh: 60000,
  }

  constructor(config) {
    this._branch = {};
    this.setConfig(config);
  }

  setConfig(config) {
    if ("object" != typeof config)
      return;
    Object.assign(this._config, config);
  }

  initializeBranch(branch) {
    [branchDir(""), branchDir(branch.key)]
      .forEach(dir =>
        fs.existsSync(dir) || fs.mkdirSync(dir)
      );

    this._branch[branch.key] = branch;
    this.saveBranch(branch);

    return this.grabAllArtifacts(branch);
  }

  deleteBranch(key) {
    return new Promise(resolve => {
      const branch = this.loadBranch(key);
      branch.artifacts
        .filter(artifact => artifact.running)
        .forEach(artifact => {
          try {
            process.kill(artifact.running.pid);
          } catch (e) {}
        });
      setTimeout(() => {
        const path = branchDir(key);
        rimraf(path, {}, console.log);
        resolve();
      }, this._config.killTimeoutSeconds * 1000);
    });
  }

  saveBranch(branch) {
    if (!fs.existsSync(branchDir(branch.key)))
      return;
    const file = branchFile(branch.key);
    const data = JSON.stringify(branch);
    fs.writeFileSync(file, data);
  }

  loadBranch(key) {
    const file = branchFile(key);
    if (!fs.existsSync(file))
      return;
    const data = fs.readFileSync(file, "utf8");
    return JSON.parse(data);
  }

  grabAllArtifacts(branch) {
    const pending = branch.artifacts
      .map((artifact, n) =>
        this.grabArtifact(branch.key, artifact)
          .then(() => {
            branch.artifacts[n].grabbed = new Date().getTime();
          })
      );
    return Promise.all(pending)
      .then(() => {
        branch.created = new Date().getTime();
        this.saveBranch(branch);
        return branch;
      });
  }

  grabArtifact(key, artifact) {
    return new Promise((resolve, reject) => {
      const path = artifactFile(key, artifact.name);
      const file = fs.createWriteStream(path);
      file.on("finish", () => file.close(resolve));
      fetch(artifact.href)
        .then(response => {
          response.body.pipe(file);
        })
        .catch(err => {
          fs.unlink(path);
          reject(err.message);
        });
    });
  }

  runArtifact(key, artifactName) {
    return new Promise(resolve => {
      const path = artifactFile(key, artifactName);
      const port = this.getPort();
      const args = [
        "-Xms" + this._config.javaXms,
        "-Xmx" + this._config.javaXmx,
        "-jar",
        path,
        `--server.port=${port}`,
      ];
      const options = {
        detached: true,
        stdio: "ignore",
      };

      const setRunning = running => {
        const branch = this.loadBranch(key);
        branch.artifacts = branch.artifacts.map(artifact =>
          artifactName == artifact.name
            ? { ...artifact, running }
            : artifact
        );
        this.saveBranch(branch);
        return branch;
      };

      console.log(`starting build ${path} (${port} ${this._config.javaXms}/${this._config.javaXmx})`)
      const ps = spawn("java", args, options)
        .on("exit", code => {
          console.log(`build stopped ${key}:${artifactName} (${code})`)
          setRunning(null);
        });
      ps.unref();

      const branch = setRunning({ pid: ps.pid, port });

      setTimeout(
        () => resolve(branch),
        this._config.runDelaySeconds * 1000
      );
    });
  }

  getPort() {
    const MIN = this._config.javaServerPortLow;
    const MAX = this._config.javaServerPortHigh;
    return MIN + Math.floor(Math.random() * (MAX - MIN));
  }

  stopArtifact(key, artifactName) {
    return new Promise(resolve => {
      const branch = this.loadBranch(key);
      const { running: { pid = 0 } } = branch.artifacts.find(x => x.name == artifactName);

      branch.artifacts = branch.artifacts.map(artifact =>
        artifactName == artifact.name
          ? { ...artifact, running: null }
          : artifact
      );
      this.saveBranch(branch);

      try {
        process.kill(pid);
      } catch (e) {}

      resolve(branch);
    });
  }

}

export default RunnerService;
